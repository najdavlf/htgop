/* Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "htgop.h"
#include "test_htgop_utils.h"

#include <rsys/math.h>
#include <string.h>

#define N 100000

static void
check_sample_quadrature
  (struct htgop* htgop,
   res_T (*get_nspecints)(const struct htgop*, size_t*),
   res_T (*get_specint)
     (const struct htgop*, const size_t, struct htgop_spectral_interval*))
{
  struct htgop_spectral_interval specint;
  size_t nspecints;
  size_t iquadpt;
  size_t i;

  CHK(htgop && get_specint);

  CHK(get_nspecints(htgop, &nspecints) == RES_OK);
  CHK(nspecints);

  CHK(get_specint(htgop, 0, &specint) == RES_OK);
  CHK(htgop_spectral_interval_sample_quadrature(NULL, 0, &iquadpt) == RES_BAD_ARG);
  CHK(htgop_spectral_interval_sample_quadrature(&specint, 1, &iquadpt) == RES_BAD_ARG);
  CHK(htgop_spectral_interval_sample_quadrature(&specint, 1, NULL) == RES_BAD_ARG);
  CHK(htgop_spectral_interval_sample_quadrature(&specint, 1, NULL) == RES_BAD_ARG);

  FOR_EACH(i, 0, 10) {
    int* hist;
    size_t ispecint;
    size_t j;

    ispecint = (size_t)(rand_canonic() * (double)nspecints);
    CHK(get_specint(htgop, ispecint, &specint) == RES_OK);

    CHK(hist = mem_calloc(specint.quadrature_length, sizeof(*hist)));
    FOR_EACH(j, 0, N) {
      const double r = rand_canonic();
      CHK(htgop_spectral_interval_sample_quadrature(&specint, r, &iquadpt) == RES_OK);
      CHK(iquadpt < specint.quadrature_length);
      CHK(specint.quadrature_cdf[iquadpt] > r);
      CHK(!iquadpt || specint.quadrature_cdf[iquadpt-1] <= r);

      hist[iquadpt] += 1;
    }

    FOR_EACH(iquadpt, 0, specint.quadrature_length) {
      if(!eq_eps(specint.quadrature_pdf[iquadpt], 0, 1.e-4))
        CHK(hist[iquadpt] > 0);
    }

    mem_rm(hist);
  }
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct htgop* htgop;
  const double* wnums;
  size_t nspecints;

  if(argc < 2) {
    fprintf(stderr, "Usage: %s FILENAME\n", argv[0]);
    return 1;
  }

  CHK(mem_init_proxy_allocator(&allocator, &mem_default_allocator) == RES_OK);

  CHK(htgop_create(NULL, &allocator, 1, &htgop) == RES_OK);
  CHK(htgop_load(htgop, argv[1]) == RES_OK);
  CHK(htgop_get_sw_spectral_intervals_count(htgop, &nspecints) == RES_OK);
  CHK(nspecints > 0);

  CHK(htgop_get_sw_spectral_intervals_wave_numbers(htgop, &wnums) == RES_OK);

  check_sample_quadrature(htgop, htgop_get_sw_spectral_intervals_count,
    htgop_get_sw_spectral_interval);
  check_sample_quadrature(htgop, htgop_get_lw_spectral_intervals_count,
    htgop_get_lw_spectral_interval);

  CHK(htgop_ref_put(htgop) == RES_OK);
  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}
