/* Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "htgop.h"
#include "htgop_c.h"

#if !defined(DATA) || !defined(DOMAIN)
  #error "Missing the <DATA|DOMAIN> macro."
#endif

#ifndef GET_DATA
  #define GET_DATA(Tab, Id) ((Tab)->CONCAT(DATA,_tab)[Id])
#endif

/*
 * Generate functions that retrieve the boundaries of the radiative properties
 */

res_T CONCAT(CONCAT(CONCAT(CONCAT(
htgop_layer_,DOMAIN),_spectral_interval_quadpoints_get_),DATA),_bounds)
  (const struct CONCAT(CONCAT(htgop_layer_,DOMAIN),_spectral_interval)* specint,
   const size_t iquad_range[2],
   const double x_h2o_range[2],
   double bounds[2])
{
  size_t iquad;
  res_T res = RES_OK;

  if(!specint || !bounds || !iquad_range) {
    res = RES_BAD_ARG;
    goto error;
  }

  if(iquad_range[0] > iquad_range[1]
  || iquad_range[0] >= specint->quadrature_length
  || iquad_range[1] >= specint->quadrature_length) {
    log_err(specint->htgop,
      "%s: invalid range of quadrature points [%lu, %lu].\n", FUNC_NAME,
      (unsigned long)iquad_range[0], (unsigned long)iquad_range[1]);
    res = RES_BAD_ARG;
    goto error;
  }

  bounds[0] = DBL_MAX;
  bounds[1] =-DBL_MAX;

  FOR_EACH(iquad, iquad_range[0], iquad_range[1]+1) {
    struct CONCAT(CONCAT(htgop_layer_,DOMAIN),_spectral_interval_tab) tab;
    double tab_bounds[2];

    HTGOP(CONCAT(CONCAT(layer_,DOMAIN),_spectral_interval_get_tab)
      (specint, iquad, &tab));
    res = CONCAT(CONCAT(CONCAT(CONCAT(
      htgop_layer_,DOMAIN),_spectral_interval_tab_get_),DATA),_bounds)
        (&tab, x_h2o_range, tab_bounds);
    if(res != RES_OK) goto exit;

    bounds[0] = MMIN(bounds[0], tab_bounds[0]);
    bounds[1] = MMAX(bounds[1], tab_bounds[1]);
  }

exit:
  return res;
error:
  if(bounds) {
    bounds[0] = DBL_MAX;
    bounds[1] =-DBL_MAX;
  }
  goto exit;
}

res_T CONCAT(CONCAT(CONCAT(CONCAT(
htgop_layer_,DOMAIN),_spectral_interval_tab_get_),DATA),_bounds)
  (const struct CONCAT(CONCAT(htgop_layer_,DOMAIN),_spectral_interval_tab)* tab,
   const double x_h2o_range[2],
   double bounds[2])
{
  size_t itab_range[2];
  double* x_h2o_low;
  double* x_h2o_upp;
  double k1, k2;
  size_t i;
  res_T res = RES_OK;

  if(!tab || !bounds || !x_h2o_range) {
    res = RES_BAD_ARG;
    goto error;
  }

  if(x_h2o_range[0] > x_h2o_range[1]) {
    log_err(tab->htgop,
      "%s: invalid water vapor molar fraction range [%g, %g].\n", 
      FUNC_NAME, SPLIT2(x_h2o_range));
    res = RES_BAD_ARG;
    goto error;
  }

  /* Find the pointers toward the first x_h2o greater than or equal to the
   * x_h2o boundaries */
  x_h2o_low = search_lower_bound(&x_h2o_range[0], tab->x_h2o_tab,
    tab->tab_length, sizeof(double), cmp_dbl);
  x_h2o_upp = search_lower_bound(&x_h2o_range[1], tab->x_h2o_tab,
    tab->tab_length, sizeof(double), cmp_dbl);
  if(!x_h2o_low || !x_h2o_upp || (x_h2o_low > x_h2o_upp)) {
    log_err(tab->htgop, "%s: invalid x_h2o range [%g, %g].\n",
      FUNC_NAME, SPLIT2(x_h2o_range));
    res = RES_BAD_ARG;
    goto error;
  }

  /* Compute the K at the boundaries of the x_h2o_range */
  k1 = CONCAT(CONCAT(CONCAT(
    layer_,DOMAIN),_spectral_interval_tab_interpolate_),DATA)
      (tab, x_h2o_range[0], x_h2o_low);

  if(x_h2o_range[0] == x_h2o_range[1]) {
    k2 = k1;
  } else {
    k2 = CONCAT(CONCAT(CONCAT(
      layer_,DOMAIN),_spectral_interval_tab_interpolate_),DATA)
        (tab, x_h2o_range[1], x_h2o_upp);
  }
  bounds[0] = MMIN(k1, k2);
  bounds[1] = MMAX(k1, k2);

  /* Define the remaining tabulation point to handle */
  itab_range[0] = (size_t)(x_h2o_low - tab->x_h2o_tab);
  itab_range[1] = (size_t)(x_h2o_upp - tab->x_h2o_tab);

  /* Iterate on the remaining tabulation point */
  FOR_EACH(i, itab_range[0], itab_range[1]) {
    const double k = GET_DATA(tab, i);
    bounds[0] = MMIN(bounds[0], k);
    bounds[1] = MMAX(bounds[1], k);
  }

exit:
  return res;
error:
  if(bounds) {
    bounds[0] = DBL_MAX;
    bounds[1] =-DBL_MAX;
  }
  goto exit;
}

#undef DATA
#undef DOMAIN
#undef GET_DATA

