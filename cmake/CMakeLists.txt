# Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.1)
project(htgop C)
enable_testing()

set(HTGOP_SOURCE_DIR ${PROJECT_SOURCE_DIR}/../src)
option(NO_TEST "Do not build tests" OFF)

################################################################################
# Check dependencies
################################################################################
find_package(RCMake 0.3 REQUIRED)
find_package(RSys 0.7 REQUIRED)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${RCMAKE_SOURCE_DIR})
include(rcmake)
include(rcmake_runtime)

include_directories(${RSys_INCLUDE_DIR})

################################################################################
# Configure and define targets
################################################################################
set(VERSION_MAJOR 0)
set(VERSION_MINOR 1)
set(VERSION_PATCH 2)
set(VERSION ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH})

set(HTGOP_FILES_SRC
  htgop.c)
set(HTGOP_FILES_INC
  htgop_c.h
  htgop_fetch_radiative_properties.h
  htgop_get_radiative_properties_bounds.h
  htgop_layer.h
  htgop_parse_layers_spectral_intervals_data.h
  htgop_reader.h
  htgop_spectral_intervals.h)
set(HTGOP_FILES_INC_API
  htgop.h)

set(HTGOP_FILES_DOC COPYING README.md)

# Prepend each file in the `HTGOP_FILES_<SRC|INC>' list by `HTGOP_SOURCE_DIR'
rcmake_prepend_path(HTGOP_FILES_SRC ${HTGOP_SOURCE_DIR})
rcmake_prepend_path(HTGOP_FILES_INC ${HTGOP_SOURCE_DIR})
rcmake_prepend_path(HTGOP_FILES_INC_API ${HTGOP_SOURCE_DIR})
rcmake_prepend_path(HTGOP_FILES_DOC ${PROJECT_SOURCE_DIR}/../)

add_library(htgop SHARED ${HTGOP_FILES_SRC} ${HTGOP_FILES_INC} ${HTGOP_FILES_INC_API})
target_link_libraries(htgop RSys)

if(CMAKE_COMPILER_IS_GNUCC)
  target_link_libraries(htgop m)
endif()

set_target_properties(htgop PROPERTIES
  DEFINE_SYMBOL HTGOP_SHARED_BUILD
  VERSION ${VERSION}
  SOVERSION ${VERSION_MAJOR})

rcmake_setup_devel(htgop HTGOP ${VERSION} high_tune/htgop_version.h)

################################################################################
# Add tests
################################################################################
if(NOT NO_TEST)
  set(HTGOP_ETC_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/etc/)
  get_filename_component(_etc_src "${PROJECT_SOURCE_DIR}/../etc.tgz" ABSOLUTE)
  add_custom_command(
    OUTPUT etc.stamp
    COMMAND ${CMAKE_COMMAND} -E tar xzf "${_etc_src}"
    COMMAND ${CMAKE_COMMAND} -E touch etc.stamp
    DEPENDS "${_etc_src}"
    COMMENT "Extract ${_etc_src}"
    VERBATIM)
  add_custom_target(extract-etc ALL DEPENDS etc.stamp)

  function(build_test _name)
    add_executable(${_name}
      ${HTGOP_SOURCE_DIR}/${_name}.c
      ${HTGOP_SOURCE_DIR}/test_htgop_utils.h)
    target_link_libraries(${_name} htgop RSys)
  endfunction()

  function(new_test _name)
    build_test(${_name})
    add_test(${_name} ${_name})
  endfunction()

  new_test(test_htgop)
  build_test(test_htgop_fetch_radiative_properties)
  build_test(test_htgop_get_radiative_properties_bounds)
  build_test(test_htgop_load)
  build_test(test_htgop_sample)

  add_test(test_htgop_fetch_radiative_properties
    test_htgop_fetch_radiative_properties
    ${HTGOP_ETC_DIRECTORY}/ecrad_opt_prop.txt)
  add_test(test_htgop_get_radiative_properties_bounds
    test_htgop_get_radiative_properties_bounds
    ${HTGOP_ETC_DIRECTORY}/ecrad_opt_prop.txt)
  add_test(test_htgop_load
    test_htgop_load ${HTGOP_ETC_DIRECTORY}/ecrad_opt_prop.txt)
  add_test(test_htgop_sample
    test_htgop_sample ${HTGOP_ETC_DIRECTORY}/ecrad_opt_prop.txt)

endif()

################################################################################
# Define output & install directories
################################################################################
install(TARGETS htgop
  ARCHIVE DESTINATION bin
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin)
install(FILES ${HTGOP_FILES_INC_API} DESTINATION include/high_tune)
install(FILES ${HTGOP_FILES_DOC} DESTINATION share/doc/htgop)

