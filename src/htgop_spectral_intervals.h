/* Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef HTGOP_SPECTRAL_INTERVALS_H
#define HTGOP_SPECTRAL_INTERVALS_H

#include "htgop_dbllst.h"

struct spectral_intervals {
  /* List of wave numbers, in cm^-1, sorted in ascending order.
   * #wave numbers == #intervals + 1 */
  struct darray_double wave_numbers;
  struct darray_dbllst quadrature_pdfs; /* Per spectral band quadrature pdf */
  struct darray_dbllst quadrature_cdfs; /* Per spectral band quadrature cdf */
};

static INLINE void
spectral_intervals_init
  (struct mem_allocator* allocator, struct spectral_intervals* sinters)
{
  ASSERT(sinters);
  darray_double_init(allocator, &sinters->wave_numbers);
  darray_dbllst_init(allocator, &sinters->quadrature_pdfs);
  darray_dbllst_init(allocator, &sinters->quadrature_cdfs);

}

static INLINE void
spectral_intervals_release(struct spectral_intervals* sinters)
{
  ASSERT(sinters);
  darray_double_release(&sinters->wave_numbers);
  darray_dbllst_release(&sinters->quadrature_pdfs);
  darray_dbllst_release(&sinters->quadrature_cdfs);
}

static INLINE res_T
spectral_intervals_copy
  (struct spectral_intervals* dst, const struct spectral_intervals* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);
  #define CALL(Func) { if(RES_OK != (res = Func)) return res; } (void)0
  CALL(darray_double_copy(&dst->wave_numbers, &src->wave_numbers));
  CALL(darray_dbllst_copy(&dst->quadrature_pdfs, &src->quadrature_pdfs));
  CALL(darray_dbllst_copy(&dst->quadrature_cdfs, &src->quadrature_cdfs));
  #undef CALL
  return RES_OK;
}

static INLINE res_T
spectral_intervals_copy_and_release
  (struct spectral_intervals* dst, struct spectral_intervals* src)
{
  res_T res = RES_OK;
  ASSERT(dst && src);
  #define CALL(Func) { if(RES_OK != (res = Func)) return res; } (void)0
  CALL(darray_double_copy_and_release(&dst->wave_numbers, &src->wave_numbers));
  CALL(darray_dbllst_copy_and_release(&dst->quadrature_pdfs, &src->quadrature_pdfs));
  CALL(darray_dbllst_copy_and_release(&dst->quadrature_pdfs, &src->quadrature_cdfs));
  #undef CALL
  spectral_intervals_release(src);
  return RES_OK;
}

#endif /* HTGOP_SPECTRAL_INTERVALS_H */

