/* Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "htgop.h"

#if !defined(DOMAIN)
  #error "Missing the <DATA|DOMAIN> macro."
#endif

/* Helper macros */
#define GET_SPECINTS                                                           \
  CONCAT(CONCAT(htgop_get_, DOMAIN), _spectral_intervals)
#define GET_NSPECINTS                                                          \
  CONCAT(CONCAT(htgop_get_, DOMAIN), _spectral_intervals_count)
#define GET_SPECINT                                                            \
  CONCAT(CONCAT(htgop_get_, DOMAIN), _spectral_interval)

static void
CONCAT(CONCAT(check_, DOMAIN), _specints)
  (const struct htgop* htgop,
   const double low, /* In wavenumber */
   const double upp) /* In wavenumber */
{
  struct htgop_spectral_interval specint_low;
  struct htgop_spectral_interval specint_upp;
  double wnums[2];
  size_t range[2];
  size_t range2[2];
  size_t i;
  size_t nspecints;

  CHK(low <= upp);

  wnums[0] = low;
  wnums[1] = upp;
  CHK(GET_SPECINTS(NULL, wnums, range) == RES_BAD_ARG);
  CHK(GET_SPECINTS(htgop, NULL, range) == RES_BAD_ARG);
  CHK(GET_SPECINTS(htgop, wnums, NULL) == RES_BAD_ARG);
  CHK(GET_SPECINTS(htgop, wnums, range) == RES_OK);
  CHK(range[0] <= range[1]);

  if(upp != low) {
    wnums[0] = upp;
    wnums[1] = low;
    CHK(GET_SPECINTS(htgop, wnums, range) == RES_BAD_ARG);
  }
  wnums[0] = low;
  wnums[1] = upp;
  CHK(GET_SPECINTS(htgop, wnums, range) == RES_OK);

  CHK(GET_SPECINT(htgop, range[0], &specint_low) == RES_OK);
  CHK(GET_SPECINT(htgop, range[1], &specint_upp) == RES_OK);
  CHK(GET_NSPECINTS(htgop, &nspecints) == RES_OK);

  CHK(specint_low.wave_numbers[0] < specint_upp.wave_numbers[1]);
  CHK(specint_low.wave_numbers[0] <= wnums[0] || range[0] == 0);
  CHK(specint_upp.wave_numbers[1] >= wnums[1] || range[1] == nspecints-1);

  range2[0] = SIZE_MAX;
  range2[1] = SIZE_MAX;
  FOR_EACH(i, 0, nspecints) {
    struct htgop_spectral_interval specint;
    CHK(GET_SPECINT(htgop, i, &specint) == RES_OK);

    if(specint.wave_numbers[0]<=wnums[0] && specint.wave_numbers[1]>wnums[0]) {
      range2[0] = i;
    }
    if(specint.wave_numbers[0]<wnums[1] && specint.wave_numbers[1]>=wnums[1]) {
      range2[1] = i;
    }
  }

  CHK(range2[0] <= range2[1]);

  if(range2[0] == SIZE_MAX) {
    /* The loaded data does not strictly include the submitted range */
    CHK(range[0] == 0);
  } else {
    CHK(range2[0] == range[0]);
  }

  if(range2[1] == SIZE_MAX) {
   /* The loaded data does not strictly include the submitted range */
    CHK(range[1] == nspecints-1);
  } else {
    CHK(range2[1] == range[1]);
  }
}

#undef GET_SPECINT
#undef GET_SPECINTS
#undef GET_NSPECINTS
#undef DOMAIN

