/* Copyright (C) 2018-2021 |Meso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* nextafter support */

#include "htgop.h"
#include "htgop_reader.h"

#include "test_htgop_utils.h"

#include <rsys/cstr.h>

static void
check_position_to_layer_id(const struct htgop* htgop)
{
  struct htgop_level lvl0, lvl1;
  const size_t N = 10000;
  double org, sz;
  size_t ilay;
  size_t i, n;

  CHK(htgop_get_levels_count(htgop, &n) == RES_OK);
  CHK(n > 1);

  /* Test the htgop_position_to_layer_id function */
  CHK(htgop_get_level(htgop, 0, &lvl0) == RES_OK);
  CHK(htgop_get_level(htgop, n-1, &lvl1) == RES_OK);
  org = lvl0.height;
  sz = lvl1.height - lvl0.height;

  CHK(htgop_position_to_layer_id(NULL, lvl0.height, &ilay) == RES_BAD_ARG);
  CHK(htgop_position_to_layer_id
    (htgop, nextafter(lvl0.height,-DBL_MAX), &ilay) == RES_BAD_ARG);
  CHK(htgop_position_to_layer_id
    (htgop, nextafter(lvl1.height, DBL_MAX), &ilay) == RES_BAD_ARG);
  CHK(htgop_position_to_layer_id(htgop, lvl0.height, NULL) == RES_BAD_ARG);

  CHK(htgop_position_to_layer_id(htgop, lvl0.height, &ilay) == RES_OK);
  CHK(ilay == 0);
  CHK(htgop_position_to_layer_id(htgop, lvl1.height, &ilay) == RES_OK);
  CHK(ilay == n - 2);

  FOR_EACH(i, 0, N) {
    const double r = rand_canonic();
    const double pos = org + r * sz;

    CHK(htgop_position_to_layer_id(htgop, pos, &ilay) == RES_OK);
    CHK(htgop_get_level(htgop, ilay+0, &lvl0) == RES_OK);
    CHK(htgop_get_level(htgop, ilay+1, &lvl1) == RES_OK);
    CHK(pos >= lvl0.height);
    CHK(pos <= lvl1.height);
  }
}

#define DOMAIN sw
#include "test_htgop_check_specints.h"

#define DOMAIN lw
#include "test_htgop_check_specints.h"

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct reader rdr;
  struct htgop* htgop;
  struct htgop_ground ground;
  struct htgop_layer lay;
  struct htgop_level lvl;
  struct htgop_spectral_interval specint;
  struct htgop_layer_lw_spectral_interval lw_specint;
  struct htgop_layer_lw_spectral_interval_tab lw_tab;
  struct htgop_layer_sw_spectral_interval sw_specint;
  struct htgop_layer_sw_spectral_interval_tab sw_tab;
  size_t nlays, nlvls, nspecints_lw, nspecints_sw;
  size_t ilay, ilvl, itab, iquad, ispecint;
  size_t tab_len;
  const double* wnums;
  FILE* fp;
  unsigned long ul;
  double dbl;

  if(argc < 2) {
    fprintf(stderr, "Usage: %s FILENAME\n", argv[0]);
    return 1;
  }

  CHK(mem_init_proxy_allocator(&allocator, &mem_default_allocator) == RES_OK);

  CHK((fp = fopen(argv[1], "r")) != NULL);
  reader_init(&rdr, fp, argv[1]);

  CHK(htgop_create(NULL, &allocator, 1, &htgop) == RES_OK);
  CHK(htgop_load(NULL, NULL) == RES_BAD_ARG);
  CHK(htgop_load(htgop, NULL) == RES_BAD_ARG);
  CHK(htgop_load(NULL, argv[1]) == RES_BAD_ARG);
  CHK(htgop_load(htgop, argv[1]) == RES_OK);

  /* #levels */
  CHK(htgop_get_levels_count(NULL, NULL) == RES_BAD_ARG);
  CHK(htgop_get_levels_count(htgop, NULL) == RES_BAD_ARG);
  CHK(htgop_get_levels_count(NULL, &nlvls) == RES_BAD_ARG);
  CHK(htgop_get_levels_count(htgop, &nlvls) == RES_OK);
  CHK(cstr_to_ulong(read_line(&rdr), &ul) == RES_OK);
  CHK(nlvls == ul);

  /* #layers */
  CHK(htgop_get_layers_count(NULL, NULL) == RES_BAD_ARG);
  CHK(htgop_get_layers_count(htgop, NULL) == RES_BAD_ARG);
  CHK(htgop_get_layers_count(NULL, &nlays) == RES_BAD_ARG);
  CHK(htgop_get_layers_count(htgop, &nlays) == RES_OK);
  CHK(cstr_to_ulong(read_line(&rdr), &ul) == RES_OK);
  CHK(nlays == ul);

  /* Ground temperature */
  CHK(htgop_get_ground(NULL, NULL) == RES_BAD_ARG);
  CHK(htgop_get_ground(htgop, NULL) == RES_BAD_ARG);
  CHK(htgop_get_ground(NULL, &ground) == RES_BAD_ARG);
  CHK(htgop_get_ground(htgop, &ground) == RES_OK);
  CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
  CHK(ground.temperature == dbl);

  /* Per level pressure */
  CHK(htgop_get_level(NULL, nlvls, NULL) == RES_BAD_ARG);
  CHK(htgop_get_level(htgop, nlvls, NULL) == RES_BAD_ARG);
  CHK(htgop_get_level(NULL, 0, NULL) == RES_BAD_ARG);
  CHK(htgop_get_level(htgop, 0, NULL) == RES_BAD_ARG);
  CHK(htgop_get_level(NULL, nlvls, &lvl) == RES_BAD_ARG);
  CHK(htgop_get_level(htgop, nlvls, &lvl) == RES_BAD_ARG);
  CHK(htgop_get_level(NULL, 0, &lvl) == RES_BAD_ARG);
  FOR_EACH(ilvl, 0, nlvls) {
    CHK(htgop_get_level(htgop, ilvl, &lvl) == RES_OK);
    CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
    CHK(dbl == lvl.pressure);
  }
  /* Per level temperature */
  FOR_EACH(ilvl, 0, nlvls) {
    CHK(htgop_get_level(htgop, ilvl, &lvl) == RES_OK);
    CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
    CHK(dbl == lvl.temperature);
  }
  /* Per level height */
  FOR_EACH(ilvl, 0, nlvls) {
    CHK(htgop_get_level(htgop, ilvl, &lvl) == RES_OK);
    CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
    CHK(dbl == lvl.height);
    if(ilvl) { /* Check strict ascending order */
      CHK(htgop_get_level(htgop, ilvl-1, &lvl) == RES_OK);
      CHK(lvl.height < dbl);
    }
  }

  /* Per layer nominal xH2O */
  CHK(htgop_get_layer(NULL, nlays, NULL) == RES_BAD_ARG);
  CHK(htgop_get_layer(htgop, nlays, NULL) == RES_BAD_ARG);
  CHK(htgop_get_layer(NULL, 0, NULL) == RES_BAD_ARG);
  CHK(htgop_get_layer(htgop, 0, NULL) == RES_BAD_ARG);
  CHK(htgop_get_layer(NULL, nlays, &lay) == RES_BAD_ARG);
  CHK(htgop_get_layer(htgop, nlays, &lay) == RES_BAD_ARG);
  CHK(htgop_get_layer(NULL, 0, &lay) == RES_BAD_ARG);
  FOR_EACH(ilay, 0, nlays) {
    CHK(htgop_get_layer(htgop, ilay, &lay) == RES_OK);
    CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
    CHK(dbl == lay.x_h2o_nominal);
  }

  /* # tabulated xH2O for each layer */
  tab_len = lay.tab_length;
  CHK(cstr_to_ulong(read_line(&rdr), &ul) == RES_OK);
  CHK(tab_len == ul);
  FOR_EACH(itab, 0, tab_len) {
    FOR_EACH(ilay, 0, nlays) {
      CHK(htgop_get_layer(htgop, ilay, &lay) == RES_OK);
      CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
      CHK(dbl == lay.x_h2o_tab[itab]);
      /* Check strict ascending order */
      CHK(!itab || lay.x_h2o_tab[itab] > lay.x_h2o_tab[itab-1]);
    }
  }

  /* Ground emissivity */
  CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
  CHK(dbl == ground.lw_emissivity);
  CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
  CHK(dbl == ground.sw_emissivity);

  /* #long wave spectral intervals */
  CHK(htgop_get_lw_spectral_intervals_count(NULL, NULL) == RES_BAD_ARG);
  CHK(htgop_get_lw_spectral_intervals_count(htgop, NULL) == RES_BAD_ARG);
  CHK(htgop_get_lw_spectral_intervals_count(NULL, &nspecints_lw) == RES_BAD_ARG);
  CHK(htgop_get_lw_spectral_intervals_count(htgop, &nspecints_lw) == RES_OK);
  CHK(cstr_to_ulong(read_line(&rdr), &ul) == RES_OK);
  CHK(ul == nspecints_lw);

  /* #short wave spectral intervals */
  CHK(htgop_get_sw_spectral_intervals_count(NULL, NULL) == RES_BAD_ARG);
  CHK(htgop_get_sw_spectral_intervals_count(htgop, NULL) == RES_BAD_ARG);
  CHK(htgop_get_sw_spectral_intervals_count(NULL, &nspecints_sw) == RES_BAD_ARG);
  CHK(htgop_get_sw_spectral_intervals_count(htgop, &nspecints_sw) == RES_OK);
  CHK(cstr_to_ulong(read_line(&rdr), &ul) == RES_OK);
  CHK(ul == nspecints_sw);

  /* Search for a LW spectral interval */
  CHK(htgop_find_lw_spectral_interval_id(NULL, 0, &ispecint) == RES_BAD_ARG);
  CHK(htgop_find_lw_spectral_interval_id(htgop, 0, NULL) == RES_BAD_ARG);
  CHK(htgop_find_lw_spectral_interval_id(htgop, 0, &ispecint) == RES_OK);
  CHK(ispecint == SIZE_MAX);

  /* Search for a SW spectral interval */
  CHK(htgop_find_sw_spectral_interval_id(NULL, 0, &ispecint) == RES_BAD_ARG);
  CHK(htgop_find_sw_spectral_interval_id(htgop, 0, NULL) == RES_BAD_ARG);
  CHK(htgop_find_sw_spectral_interval_id(htgop, 0, &ispecint) == RES_OK);
  CHK(ispecint == SIZE_MAX);

  /* Per LW spectral interval data */
  CHK(htgop_get_lw_spectral_intervals_wave_numbers(NULL, NULL) == RES_BAD_ARG);
  CHK(htgop_get_lw_spectral_intervals_wave_numbers(htgop, NULL) == RES_BAD_ARG);
  CHK(htgop_get_lw_spectral_intervals_wave_numbers(NULL, &wnums) == RES_BAD_ARG);
  CHK(htgop_get_lw_spectral_intervals_wave_numbers(htgop, &wnums) == RES_OK);
  CHK(htgop_get_lw_spectral_interval(NULL, nspecints_lw, NULL) == RES_BAD_ARG);
  CHK(htgop_get_lw_spectral_interval(htgop, nspecints_lw, NULL) == RES_BAD_ARG);
  CHK(htgop_get_lw_spectral_interval(NULL, 0, NULL) == RES_BAD_ARG);
  CHK(htgop_get_lw_spectral_interval(htgop, 0, NULL) == RES_BAD_ARG);
  CHK(htgop_get_lw_spectral_interval(NULL, nspecints_lw, &specint) == RES_BAD_ARG);
  CHK(htgop_get_lw_spectral_interval(htgop, nspecints_lw, &specint) == RES_BAD_ARG);
  CHK(htgop_get_lw_spectral_interval(NULL, 0, &specint) == RES_BAD_ARG);
  FOR_EACH(ispecint, 0, nspecints_lw) {
    size_t i;
    CHK(htgop_get_lw_spectral_interval(htgop, ispecint, &specint) == RES_OK);
    CHK(specint.wave_numbers[0] == wnums[ispecint+0]);
    CHK(specint.wave_numbers[1] == wnums[ispecint+1]);

    /* Check the finding of a LW spectral interval */
    FOR_EACH(i, 0, 10) {
      double wnum;
      size_t id;
      wnum = wnums[ispecint+0]
           + rand_canonic() * (wnums[ispecint+1] - wnums[ispecint+0]);
      CHK(htgop_find_lw_spectral_interval_id(htgop, wnum, &id) == RES_OK);
      CHK(id == ispecint);
    }

    CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
    CHK(specint.wave_numbers[0] == dbl);
    CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
    CHK(specint.wave_numbers[1] == dbl);
    CHK(cstr_to_ulong(read_line(&rdr), &ul) == RES_OK);
    CHK(specint.quadrature_length == ul);

    FOR_EACH(iquad, 0, specint.quadrature_length) {
      CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
      CHK(specint.quadrature_pdf[iquad] == dbl);
      if(iquad == 0) {
        CHK(specint.quadrature_cdf[iquad] == dbl);
      } else {
        const double pdf =
          specint.quadrature_cdf[iquad] - specint.quadrature_cdf[iquad-1];
        CHK(eq_eps(pdf, dbl, 1.e-6));
      }
    }
    CHK(specint.quadrature_cdf[specint.quadrature_length-1] == 1.0);
  }

  /* Per SW spectral interval data */
  CHK(htgop_get_sw_spectral_intervals_wave_numbers(NULL, NULL) == RES_BAD_ARG);
  CHK(htgop_get_sw_spectral_intervals_wave_numbers(htgop, NULL) == RES_BAD_ARG);
  CHK(htgop_get_sw_spectral_intervals_wave_numbers(NULL, &wnums) == RES_BAD_ARG);
  CHK(htgop_get_sw_spectral_intervals_wave_numbers(htgop, &wnums) == RES_OK);
  CHK(htgop_get_sw_spectral_interval(NULL, nspecints_sw, NULL) == RES_BAD_ARG);
  CHK(htgop_get_sw_spectral_interval(htgop, nspecints_sw, NULL) == RES_BAD_ARG);
  CHK(htgop_get_sw_spectral_interval(NULL, 0, NULL) == RES_BAD_ARG);
  CHK(htgop_get_sw_spectral_interval(htgop, 0, NULL) == RES_BAD_ARG);
  CHK(htgop_get_sw_spectral_interval(NULL, nspecints_sw, &specint) == RES_BAD_ARG);
  CHK(htgop_get_sw_spectral_interval(htgop, nspecints_sw, &specint) == RES_BAD_ARG);
  CHK(htgop_get_sw_spectral_interval(NULL, 0, &specint) == RES_BAD_ARG);
  FOR_EACH(ispecint, 0, nspecints_sw) {
    int i;
    CHK(htgop_get_sw_spectral_interval(htgop, ispecint, &specint) == RES_OK);
    CHK(specint.wave_numbers[0] == wnums[ispecint+0]);
    CHK(specint.wave_numbers[1] == wnums[ispecint+1]);

    /* Check the finding of a SW spectral interval */
    FOR_EACH(i, 0, 10) {
      double wnum;
      size_t id;
      wnum = wnums[ispecint+0]
           + rand_canonic() * (wnums[ispecint+1] - wnums[ispecint+0]);
      CHK(htgop_find_sw_spectral_interval_id(htgop, wnum, &id) == RES_OK);
      CHK(id == ispecint);
    }

    CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
    CHK(specint.wave_numbers[0] == dbl);
    CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
    CHK(specint.wave_numbers[1] == dbl);
    CHK(cstr_to_ulong(read_line(&rdr), &ul) == RES_OK);
    CHK(specint.quadrature_length == ul);

    FOR_EACH(iquad, 0, specint.quadrature_length) {
      CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
      CHK(specint.quadrature_pdf[iquad] == dbl);
      if(iquad == 0) {
        CHK(specint.quadrature_cdf[iquad] == dbl);
      } else {
        const double pdf =
          specint.quadrature_cdf[iquad] - specint.quadrature_cdf[iquad-1];
        CHK(eq_eps(pdf, dbl, 1.e-6));
      }
      CHK(specint.quadrature_cdf[specint.quadrature_length-1] == 1.0);
    }
  }

  /* Per layer LW ka data */
  FOR_EACH(ispecint, 0, nspecints_lw) {
    CHK(htgop_get_lw_spectral_interval(htgop, ispecint, &specint) == RES_OK);
    FOR_EACH(iquad, 0, specint.quadrature_length) {
      FOR_EACH(ilay, 0, nlays) {
        CHK(htgop_get_layer(htgop, ilay, &lay) == RES_OK);
        CHK(lay.lw_spectral_intervals_count == nspecints_lw);
        CHK(lay.sw_spectral_intervals_count == nspecints_sw);
        CHK(htgop_layer_get_lw_spectral_interval(&lay, ispecint, &lw_specint) == RES_OK);
        CHK(lw_specint.quadrature_length == specint.quadrature_length);
        CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
        CHK(lw_specint.ka_nominal[iquad] == dbl);
      }
      FOR_EACH(itab, 0, tab_len) {
        FOR_EACH(ilay, 0, nlays) {
          CHK(htgop_get_layer(htgop, ilay, &lay) == RES_OK);
          CHK(lay.lw_spectral_intervals_count == nspecints_lw);
          CHK(lay.sw_spectral_intervals_count == nspecints_sw);
          CHK(htgop_layer_get_lw_spectral_interval(&lay, ispecint, &lw_specint) == RES_OK);
          CHK(htgop_layer_lw_spectral_interval_get_tab(&lw_specint, iquad, &lw_tab) == RES_OK);
          CHK(lw_tab.tab_length == tab_len);
          CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
          CHK(lw_tab.ka_tab[itab] == dbl);
          CHK(lw_tab.x_h2o_tab[itab] == lay.x_h2o_tab[itab]);
        }
      }
    }
  }

  /* Per layer SW ka data */
  FOR_EACH(ispecint, 0, nspecints_sw) {
    CHK(htgop_get_sw_spectral_interval(htgop, ispecint, &specint) == RES_OK);
    FOR_EACH(iquad, 0, specint.quadrature_length) {
      FOR_EACH(ilay, 0, nlays) {
        CHK(htgop_get_layer(htgop, ilay, &lay) == RES_OK);
        CHK(htgop_layer_get_sw_spectral_interval(&lay, ispecint, &sw_specint) == RES_OK);
        CHK(sw_specint.quadrature_length == specint.quadrature_length);
        CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
        CHK(sw_specint.ka_nominal[iquad] == dbl);
      }
      FOR_EACH(itab, 0, tab_len) {
        FOR_EACH(ilay, 0, nlays) {
          CHK(htgop_get_layer(htgop, ilay, &lay) == RES_OK);
          CHK(htgop_layer_get_sw_spectral_interval(&lay, ispecint, &sw_specint) == RES_OK);
          CHK(htgop_layer_sw_spectral_interval_get_tab(&sw_specint, iquad, &sw_tab) == RES_OK);
          CHK(sw_tab.tab_length == tab_len);
          CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
          CHK(sw_tab.ka_tab[itab] == dbl);
          CHK(sw_tab.x_h2o_tab[itab] == lay.x_h2o_tab[itab]);
        }
      }
    }
  }

  /* Per layer SW ks data */
  FOR_EACH(ispecint, 0, nspecints_sw) {
    CHK(htgop_get_sw_spectral_interval(htgop, ispecint, &specint) == RES_OK);
    FOR_EACH(iquad, 0, specint.quadrature_length) {
      FOR_EACH(ilay, 0, nlays) {
        CHK(htgop_get_layer(htgop, ilay, &lay) == RES_OK);
        CHK(htgop_layer_get_sw_spectral_interval(&lay, ispecint, &sw_specint) == RES_OK);
        CHK(sw_specint.quadrature_length == specint.quadrature_length);
        CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
        CHK(sw_specint.ks_nominal[iquad] == dbl);
      }
      FOR_EACH(itab, 0, tab_len) {
        FOR_EACH(ilay, 0, nlays) {
          CHK(htgop_get_layer(htgop, ilay, &lay) == RES_OK);
          CHK(htgop_layer_get_sw_spectral_interval(&lay, ispecint, &sw_specint) == RES_OK);
          CHK(htgop_layer_sw_spectral_interval_get_tab(&sw_specint, iquad, &sw_tab) == RES_OK);
          CHK(sw_tab.tab_length == tab_len);
          CHK(cstr_to_double(read_line(&rdr), &dbl) == RES_OK);
          CHK(sw_tab.ks_tab[itab] == dbl);
          CHK(sw_tab.x_h2o_tab[itab] == lay.x_h2o_tab[itab]);
        }
      }
    }
  }

  check_position_to_layer_id(htgop);
  check_sw_specints(htgop, 1.e7/780.0, 1.e7/380.0);
  check_lw_specints(htgop, 1.e7/100000, 1.e7/1000);
  check_lw_specints(htgop, 1.e7/5600, 1.e7/3000);
  check_lw_specints(htgop, 1.e7/9500, 1.e7/9300);
  check_lw_specints(htgop, 1.e7/5000, 1.e7/5000);

  CHK(fclose(fp) == 0);
  reader_release(&rdr);
  CHK(htgop_ref_put(htgop) == RES_OK);
  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}

